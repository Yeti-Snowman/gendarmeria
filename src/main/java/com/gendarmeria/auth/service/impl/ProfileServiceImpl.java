package com.gendarmeria.auth.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.gendarmeria.auth.dto.ProfileDTO;
import com.gendarmeria.auth.service.ProfileService;

public class ProfileServiceImpl implements ProfileService {

	@Override
	public List<ProfileDTO> getProfiles(String token) {
		List<ProfileDTO> response = new ArrayList<>();
		ProfileDTO admin = new ProfileDTO();
		admin.setId(1);
		admin.setName("admin");
		response.add(admin);

		return response;
	}

}
