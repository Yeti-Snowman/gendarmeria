package com.gendarmeria.auth.service;

import java.util.List;

import com.gendarmeria.auth.dto.ProfileDTO;

public interface ProfileService {

	List<ProfileDTO> getProfiles(String token);

}
