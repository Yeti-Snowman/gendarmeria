package com.gendarmeria.auth.service;

import com.gendarmeria.auth.dto.LoginDTO;
import com.gendarmeria.auth.dto.TokenDTO;

public interface TokenService {

	TokenDTO login(LoginDTO body);

}
