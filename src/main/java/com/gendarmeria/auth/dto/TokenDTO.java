package com.gendarmeria.auth.dto;

public class TokenDTO {
	private String token;
	
	public TokenDTO() {
		this.token= "";	
	}
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
