package com.gendarmeria.auth.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gendarmeria.auth.dto.LoginDTO;
import com.gendarmeria.auth.dto.ProfileDTO;
import com.gendarmeria.auth.dto.TokenDTO;
import com.gendarmeria.auth.service.ProfileService;
import com.gendarmeria.auth.service.TokenService;
import com.gendarmeria.auth.service.impl.ProfileServiceImpl;
import com.gendarmeria.auth.service.impl.TokenServiceImpl;

@RestController
@RequestMapping(value = "/auth")
public class AuthController {

	private TokenService tokenService;

	private ProfileService profileService;

	public AuthController() {
		this.tokenService = new TokenServiceImpl();
		this.profileService = new ProfileServiceImpl();
	}

	@PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TokenDTO> login(@RequestBody LoginDTO body) {
		TokenDTO token = this.tokenService.login(body);
		return ResponseEntity.ok(token);
	}

	@GetMapping(value = "/profile/{token}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProfileDTO>> getProfiles(@PathParam(value = "token") String token) {
		List<ProfileDTO> profiles = this.profileService.getProfiles(token);
		return new ResponseEntity<>(profiles, HttpStatus.OK);
	}
}
